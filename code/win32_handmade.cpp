/* =====================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Instructor: Casey Muratori $
   $Apprentice: Andrew Pankow $
   $Notes: Graphic Games Programming - Handmade Hero $
   $Notice: (C) Copyright 2014 by Molly Rocket, Inc. All Rights Reserved. $
======================================================================================== */
#include <stdint.h>


// TODO(andrew): Look up lectures by Chandler Curruth & LLVM

/* Scoping */
#define internal        static
#define local_persist   static
#define global_variable static



#if _WIN32 // MICROSOFT WINDOWS SPECIFIC CODE

#include <windows.h>
#include <xinput.h>
#include <dsound.h>



/* Special Types */
struct Win32_Offscreen_Buffer
{
// NOTE(andrew): Pixels are always 32 bits wide, Memory-order = 0xAARRGGBB
	BITMAPINFO Info;   // Info about our buffer
	void *Memory;      // Our actual buffer
	int Width;         // Won't be a global var later
	int Height;        // Won't be a global var later
	int Pitch;         // Size of each row in bytes
} typedef Win32_Offscreen_Buffer;

struct Win32_Window_Dimension
{
	int Width;
	int Height;
} typedef Win32_Window_Dimension;




/* Global Variables */
// TODO(andrew):	Global for now; will want to let user have more control later
global_variable bool GlobalRunning;
global_variable Win32_Offscreen_Buffer GlobalBackBuffer;
global_variable LPDIRECTSOUNDBUFFER GlobalSoundBuffer;


/*
	define a call-prototype for function
	create a custom-type for the funtion with the call-prototype
	define a default function just in case can't load real one with the call-prototype
	create a pointer to the call-prototype of the function using the custom-type 
			and set it to our default function
	define a preprocessor directive for the Window's API call to alias to our function pointer
*/
#define X_INPUT_GET_STATE( name ) DWORD name( DWORD dwUserIndex, XINPUT_STATE *pState )
typedef X_INPUT_GET_STATE( get_x_input_state );
X_INPUT_GET_STATE( XInputGetStateStub )
{
	return( 0 );
}
global_variable get_x_input_state *XInputGetState_ = XInputGetStateStub;
#define XInputGetState XInputGetState_


#define X_INPUT_SET_STATE( name ) DWORD name( DWORD dwUserIndex, XINPUT_VIBRATION *pVibration )
typedef X_INPUT_SET_STATE( set_x_input_state );
X_INPUT_SET_STATE( XInputSetStateStub )
{
	return( 0 );
}
global_variable set_x_input_state *XInputSetState_ = XInputSetStateStub;
#define XInputSetState XInputSetState_



#define DIRECT_SOUND_CREATE(name) HRESULT WINAPI name( LPCGUID pcGuidDevice, LPDIRECTSOUND *ppDS, LPUNKNOWN lpUnkOuter )
typedef DIRECT_SOUND_CREATE(direct_sound_create);

/* Functions */
internal void Win32LoadXInput( void )
{
	HMODULE XInputLibrary = LoadLibraryA( "xinput1_4.dll" );
	if( !XInputLibrary )
	{
		XInputLibrary = LoadLibraryA( "xinput1_3.dll" );
	}
	if( XInputLibrary )
	{
		XInputGetState = (get_x_input_state *)GetProcAddress( XInputLibrary, "XInputGetState" );
		XInputSetState = (set_x_input_state *)GetProcAddress( XInputLibrary, "XInputSetState" );
	}
}


internal void Win32InitDirectSount( HWND Window, int32_t SamplesPerSecond, int32_t BufferSize )
{
	HMODULE DirectSoundLibrary = LoadLibraryA( "dsound.dll" );
	
	if( DirectSoundLibrary )
	{
		// Get a Direct Sound Object
		direct_sound_create *DirectSoundCreate = 
			(direct_sound_create *)GetProcAddress( DirectSoundLibrary, "DirectSoundCreate" );
		
		LPDIRECTSOUND DirectSound;
		if( DirectSoundCreate && SUCCEEDED(DirectSoundCreate( 0, &DirectSound, 0 )) )
		{
			WAVEFORMATEX WaveFormat = {};
			
			WaveFormat.wFormatTag      = WAVE_FORMAT_PCM;
			WaveFormat.nChannels       = 2;
			WaveFormat.nSamplesPerSec  = SamplesPerSecond;
			WaveFormat.wBitsPerSample  = 16;
			WaveFormat.nBlockAlign     = ( WaveFormat.nChannels * WaveFormat.wBitsPerSample ) / 8;
			WaveFormat.nAvgBytesPerSec = WaveFormat.nSamplesPerSec * WaveFormat.nBlockAlign;
			WaveFormat.cbSize          = 0;
			
			// "create" a control buffer
			if( SUCCEEDED(DirectSound->SetCooperativeLevel( Window, DSSCL_PRIORITY )) )
			{
				DSBUFFERDESC BufferDescription = {};
				BufferDescription.dwSize  = sizeof( BufferDescription );
				BufferDescription.dwFlags = DSBCAPS_PRIMARYBUFFER; /* the control buffer */

				LPDIRECTSOUNDBUFFER ControlBuffer;
				if( SUCCEEDED(DirectSound->CreateSoundBuffer( &BufferDescription, &ControlBuffer, 0 )) )
				{
					HRESULT Error = ControlBuffer->SetFormat( &WaveFormat );
					if( SUCCEEDED(Error) )
					{
						// The format has been set
						OutputDebugStringA( "Control Buffer (aka Primary) was set.\n" );
					}
					else
					{
//	TODO(andrew):	Diagnostic
					}
				}
				else
				{
//	TODO(andrew):	Diagnostic
				}
			}
			else
			{
//	TODO(andrew):	Diagnostic
			}

			// "create" a sound buffer
			DSBUFFERDESC BufferDescription = {};
// TODO(andrew):	DSBCAPS_GETCURRENTPOSITION2
			BufferDescription.dwSize        = sizeof( BufferDescription );
			BufferDescription.dwFlags       = 0;
			BufferDescription.dwBufferBytes = BufferSize;
			BufferDescription.lpwfxFormat   = &WaveFormat;
			
			HRESULT Error = DirectSound->CreateSoundBuffer( &BufferDescription, &GlobalSoundBuffer, 0 );
			if( SUCCEEDED(Error) )
			{
//	TODO(andrew):	start the sounds
				OutputDebugStringA( "Sound Buffer (aka Secondary) was set.\n" );
			}
			else
			{
//	TODO(andrew):	Diagnostic
			}
			
		}
		else
		{
//	TODO(andrew):	Diagnostic
		}
	}
	else
	{
//	TODO(andrew):	Diagnostic
	}
}


internal Win32_Window_Dimension Win32GetWindowDimension( HWND WindowHandle )
{ 
		Win32_Window_Dimension Result;
		
		RECT ClientRect;
		GetClientRect( WindowHandle, &ClientRect ); // Discover the size of drawable area
		Result.Width = ClientRect.right - ClientRect.left; // width
		Result.Height = ClientRect.bottom - ClientRect.top; // height
		
		return( Result );
}

internal void RenderCoolGradient( Win32_Offscreen_Buffer *Buffer, int xOffset, int yOffset )
{
// TODO(andrew): check which is better, passing by ref or by copy
	uint8_t *Row = (uint8_t *)Buffer->Memory;
	for( int Y=0; Y < Buffer->Height; ++Y )
	{
		uint32_t *Pixel = (uint32_t *)Row;
		for( int X=0; X < Buffer->Width; ++X )
		{
			/*
				Pixel: Alpha Blue Green Red
            REG:   0xAABBGGRR
			*/
			uint8_t r = 0;
			uint8_t g = (uint8_t)Y + yOffset;
			uint8_t b = (uint8_t)X + xOffset;
			// combine the channels into one value
			*Pixel++ = ( ( r << 16 ) | ( g << 8 ) | b);
		}
		Row += Buffer->Pitch; // Pitch won't always align with pixel counts
	}
}


internal void ResizeDIBSection( Win32_Offscreen_Buffer *Buffer, int Width, int Height )
{
	// just in case we need to adjust the size of the buffer
	if( Buffer->Memory )
	{
		VirtualFree( Buffer->Memory, 0, MEM_RELEASE ); // MEM_DECOMMIT would keep mem space reserved
	}

	Buffer->Width = Width;
	Buffer->Height = Height;
	int BytesPerPixel = 4;

	Buffer->Info.bmiHeader.biSize          = sizeof( Buffer->Info.bmiHeader );
	Buffer->Info.bmiHeader.biWidth         = Buffer->Width;
	Buffer->Info.bmiHeader.biHeight        = -Buffer->Height; // top-down; positive would be bottom-up
	Buffer->Info.bmiHeader.biPlanes        = 1;
	Buffer->Info.bmiHeader.biBitCount      = 32; // 24-bit RGB with 8-bit padding to keep aligned
	Buffer->Info.bmiHeader.biCompression   = BI_RGB;

	int BitmapMemorySize = ( Buffer->Width * Buffer->Height ) * BytesPerPixel;
	Buffer->Memory = VirtualAlloc( 0, BitmapMemorySize, MEM_COMMIT, PAGE_READWRITE );

// TODO(andrew):	clear to black
	Buffer->Pitch = Width * BytesPerPixel; // Size of each row in bytes
}

internal void Win32DisplayBufferInWindow( 
							  Win32_Offscreen_Buffer *Buffer
							, HDC DeviceContext
							, int WindowWidth, int WindowHeight
					)
{
// TODO(andrew):	aspect ratios
	StretchDIBits( // BitBlt entirely leaned on the Device Context
			  DeviceContext
			, 0, 0, WindowWidth, WindowHeight     // Destination Rectangle
			, 0, 0, Buffer->Width, Buffer->Height // Source Rectangle
			, Buffer->Memory                      // const VOID *lpBits
			, &Buffer->Info                       // BITMAPINFO *lpBitsInfo
			, DIB_RGB_COLORS                      // RGB Values instead of indices to colors in memory
			, SRCCOPY // https://msdn.microsoft.com/en-us/library/windows/desktop/dd183370(v=vs.85).aspx
		);

}

internal void Win32KeyboardDebug( char *button, bool IsDown, bool WasDown )
{
	OutputDebugStringA( button );
	// check if this particular button is down
	if( IsDown )
	{
		OutputDebugStringA( "IsDown " );
	}
	// check if this particular button was down last call
	if( WasDown )
	{
		OutputDebugStringA( "WasDown " );
	}
	OutputDebugStringA( "\n" );
}


/* Handling Callbacks */
internal LRESULT CALLBACK MainWindowCallback(
									  HWND   WindowHandle // The window
									, UINT   CallbackMessage // The message
									, WPARAM wParam
									, LPARAM lParam
								)
{
	LRESULT Result = 0;

	switch(CallbackMessage) // The messages that get called back to our message queue
	{
		case WM_CLOSE:
		{
// TODO(andrew):	Will need to know if user's really wanted to close
			GlobalRunning = false;
		} break;
		case WM_ACTIVATEAPP:
		{
			OutputDebugStringA("WM_ACTIVATEAPP\n");
		} break;
		case WM_DESTROY:
		{
// TODO(andrew):	Window could get destroyed accidentally; will need to know if user's decision 
			GlobalRunning = false;
		} break;
		case WM_SYSKEYDOWN:
		case WM_SYSKEYUP:
		case WM_KEYDOWN:
		case WM_KEYUP:
		{
			uint32_t VKCode = wParam;
			int32_t WasDown = (( lParam & ( 1 << 30 )) != 0); // 30th bit is flag for was down
			int32_t IsDown = (( lParam & ( 1 << 31 )) == 0); // 31st bit is flag for is down
			
			int32_t AltKeyAlsoDown =(( lParam & ( 1 << 29 )) != 0 );
			
			// check if a button was triggered (WINAPI uses caps)
			if( VKCode == 'W' )
			{
				Win32KeyboardDebug( "W ", IsDown, WasDown );
			}
			else if( VKCode == 'A' )
			{
				Win32KeyboardDebug( "A ", IsDown, WasDown );
			}
			else if( VKCode == 'S' )
			{
				Win32KeyboardDebug( "S ", IsDown, WasDown );
			}
			else if( VKCode == 'D' )
			{
				Win32KeyboardDebug( "D ", IsDown, WasDown );
			}
			else if( VKCode == 'Q' )
			{
				Win32KeyboardDebug( "Q ", IsDown, WasDown );
			}
			else if( VKCode == 'E' )
			{
				Win32KeyboardDebug( "E ", IsDown, WasDown );
			}
			else if( VKCode == VK_UP )
			{
				Win32KeyboardDebug( "UP_ARROW ", IsDown, WasDown );
			}
			else if( VKCode == VK_DOWN )
			{
				Win32KeyboardDebug( "DOWN_ARROW ", IsDown, WasDown );
			}
			else if( VKCode == VK_LEFT )
			{
				Win32KeyboardDebug( "LEFT_ARROW ", IsDown, WasDown );
			}
			else if( VKCode == VK_RIGHT )
			{
				Win32KeyboardDebug( "RIGHT_ARROW ", IsDown, WasDown );
			}
			else if( VKCode == VK_SPACE )
			{
				Win32KeyboardDebug( "SPACE_BAR ", IsDown, WasDown );
			}
			else if( VKCode == VK_ESCAPE )
			{
				Win32KeyboardDebug( "ESCAPE ", IsDown, WasDown );
			}
			else if( AltKeyAlsoDown && ( VKCode == VK_F4 ))
			{
				GlobalRunning = 0;
			}
			
		} break;
		case WM_PAINT:
		{
			/*
				Our code/techniques will do the heavy lifting then pass to GDI to display to the window
				We have to ask Windows to allocate a buffer for the renders in a format it can understand
				We will write into the buffer
				Then we will have Windows display the buffer
			*/
			PAINTSTRUCT Paint;
			HDC DeviceContext = BeginPaint( WindowHandle, &Paint );

			Win32_Window_Dimension Dim = Win32GetWindowDimension( WindowHandle );

			Win32DisplayBufferInWindow(
				  &GlobalBackBuffer
				, DeviceContext                            // The device context to update
				, Dim.Width
				, Dim.Height
			);

			EndPaint( WindowHandle, &Paint );
		} break;


		default:
		{
			// OutputDebugStringA("default\n");
			Result = DefWindowProc( WindowHandle, CallbackMessage, wParam, lParam );
		} break;
	}

	return(Result);
}



/* Entry Point -- main() -- */
int CALLBACK WinMain( // the entry point, int main(), declaration on a Windows Desktop
	  HINSTANCE hInstance            /* The current instance of the application           */
	, HINSTANCE hPrevInstance        /* Will go unused... Mostly legacy                   */
	, LPSTR lpCmdLine                /* The commandline arguments passed when app starts  */
	, int nCmdShow                   /* The status of the window
                                       Minimized, maximized, etc.                        */
)
{
	Win32LoadXInput();

	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms633576(v=vs.85).aspx
	WNDCLASSA WindowClass = {};

	ResizeDIBSection( &GlobalBackBuffer, 1280, 720 );

	WindowClass.style = CS_HREDRAW | CS_VREDRAW; // Repaint the whole window not just the new section
	WindowClass.lpfnWndProc = MainWindowCallback;

	// GetModuleHandle(); if you weren't the one to write WinMain()
	WindowClass.hInstance = hInstance;
	// WindowClass.hIcon = ;
	WindowClass.lpszClassName = "HandmadeHeroWindowClass";

	if( RegisterClassA( &WindowClass ) )
	{
		HWND WindowHandle = CreateWindowExA(
										  0                                 /*  */
										, WindowClass.lpszClassName         /*  */
										, "Handmade Hero"                   /* The window's name */
										, WS_OVERLAPPEDWINDOW | WS_VISIBLE  /*  */
										, CW_USEDEFAULT                     /* window's anchor's x-pos */
										, CW_USEDEFAULT                     /* window's anchor's y-pos */
										, CW_USEDEFAULT                     /* window's width */
										, CW_USEDEFAULT                     /* window's height */
										, 0                                 /* Windows inside of windows? No
										                                       Top level... On desktop
										                                       Zero means null in this case */ 
										, 0                                 /* For the menu
										                                       Zero means null in this case */
										, hInstance                         /* The current instance -- passed to us -- */
										, 0                                 /* An on-create special parameter */
									);
		if( WindowHandle )
		{
			HDC DeviceContext = GetDC( WindowHandle );
			
			int xOffset = 0;
			int yOffset = 0;
			
			int SamplesPerSecond        = 48000;
			int16_t ToneVolume          = 3000;
			int ToneHz                  = 261;                       // ~ middle c
			uint32_t RunningSampleIndex = 0;
			int SquareWavePeriod        = SamplesPerSecond / ToneHz; // int for square wave
			int HalfSquareWavePeriod    = SquareWavePeriod / 2;      // int for square wave
			int BytesPerSample          = sizeof( int16_t ) * 2;
			int SoundBufferSize         = SamplesPerSecond * BytesPerSample;
			
			Win32InitDirectSount( WindowHandle, SamplesPerSecond, SoundBufferSize );
			GlobalSoundBuffer->Play( 0, 0, DSBPLAY_LOOPING );
			
			
			
			GlobalRunning = true;
			while( GlobalRunning ) // compiler acceptable infinite loop
			{
				MSG Message; // a queued message object format
				
				while( PeekMessage( &Message, 0, 0, 0, PM_REMOVE ) )
				{
					if( Message.message == WM_QUIT )
					{
						GlobalRunning = false;
					}
					TranslateMessage( &Message );
					DispatchMessageA( &Message );
				}
				
// TODO(andrew):	Should we poll more frequently?
				XINPUT_VIBRATION Vibration;
				for( DWORD ControllerIndex = 0; ControllerIndex < XUSER_MAX_COUNT; ControllerIndex++ )
				{
					XINPUT_STATE ControllerState;
					if( XInputGetState( ControllerIndex, &ControllerState ) == ERROR_SUCCESS )
					{
						// NOTE(andrew):	controller is ready
						XINPUT_GAMEPAD *Pad = &ControllerState.Gamepad;
						bool Up            = ( Pad->wButtons & XINPUT_GAMEPAD_DPAD_UP );
						bool Down          = ( Pad->wButtons & XINPUT_GAMEPAD_DPAD_DOWN );
						bool Left          = ( Pad->wButtons & XINPUT_GAMEPAD_DPAD_LEFT );
						bool Right         = ( Pad->wButtons & XINPUT_GAMEPAD_DPAD_RIGHT );
						bool Start         = ( Pad->wButtons & XINPUT_GAMEPAD_START );
						bool Back          = ( Pad->wButtons & XINPUT_GAMEPAD_BACK );
						bool LeftShoulder  = ( Pad->wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER );
						bool RightShoulder = ( Pad->wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER );
						bool AButton       = ( Pad->wButtons & XINPUT_GAMEPAD_A );
						bool BButton       = ( Pad->wButtons & XINPUT_GAMEPAD_B );
						bool XButton       = ( Pad->wButtons & XINPUT_GAMEPAD_X );
						bool YButton       = ( Pad->wButtons & XINPUT_GAMEPAD_Y );
						
						int16_t StickLX    = Pad->sThumbLX;
						int16_t StickLY    = Pad->sThumbLY;
						
						xOffset += StickLX >> 12; // shifted down for speed of scrolling
						yOffset += StickLY >> 12; // shifted down for speed of scrolling
						
						/*
						if( AButton )
						{
							Vibration.wLeftMotorSpeed = 60000;
							Vibration.wRightMotorSpeed = 60000;
						}
						else
						{
							Vibration.wLeftMotorSpeed = 0;
							Vibration.wRightMotorSpeed = 0;
						}
						XInputSetState(0, &Vibration);
						*/
					}
					else
					{
						// NOTE(andrew):	controller is not available
					}
				}
				
				RenderCoolGradient( &GlobalBackBuffer, xOffset, yOffset );
				
				/*
					note(andrew):	DirectSound output test
					
					Our buffer:
					[  LEFT    RIGHT  ] [  LEFT    RIGHT  ] [  LEFT    RIGHT  ] ...
					[ int16_t int16_t ] [ int16_t int16_t ] [ int16_t int16_t ] ...
				*/ 
				DWORD PlayCursor;
				DWORD WriteCursor;
				if( SUCCEEDED(GlobalSoundBuffer->GetCurrentPosition( &PlayCursor, &WriteCursor )) )
				{
					DWORD ByteToLock = ( RunningSampleIndex * BytesPerSample ) % SoundBufferSize;
					DWORD BytesToWrite;
					
					if( ByteToLock > PlayCursor )
					{
						BytesToWrite = (SoundBufferSize - ByteToLock );
						BytesToWrite += PlayCursor;
					}
					else
					{
						BytesToWrite = PlayCursor - ByteToLock;
					}
					
					
					VOID *Region1;
					DWORD Region1Size;
					VOID *Region2;
					DWORD Region2Size;
					
					
					if( SUCCEEDED(GlobalSoundBuffer->Lock( ByteToLock, BytesToWrite
														, &Region1, &Region1Size
														, &Region2, &Region2Size
														, 0)) )
					{
						// note(andrew):	Square wave
						
						// Half of Square Wave Period as an index allows us to check if even/odd,
						//   * even (including zero) will be high
						//   * odd will be low
						
						int16_t *SampleOut = (int16_t *)Region1;
						DWORD Region1SampleCount = Region1Size / BytesPerSample;
						for( DWORD SampleIndex = 0; SampleIndex < Region1SampleCount; ++SampleIndex )
						{
							int16_t SampleValue = (( RunningSampleIndex++ / HalfSquareWavePeriod ) % 2) ? ToneVolume : -ToneVolume;
							*SampleOut++ = SampleValue;
							*SampleOut++ = SampleValue;
						}
						
						SampleOut = (int16_t *)Region2;
						DWORD Region2SampleCount = Region2Size / BytesPerSample;
						for( DWORD SampleIndex = 0; SampleIndex < Region2SampleCount; ++SampleIndex )
						{
							int16_t SampleValue = (( RunningSampleIndex++ / HalfSquareWavePeriod ) % 2) ? ToneVolume : -ToneVolume;
							*SampleOut++ = SampleValue;
							*SampleOut++ = SampleValue;
						}
						GlobalSoundBuffer->Unlock( Region1, Region1Size, Region2, Region2Size );
					}
				}
				Win32_Window_Dimension Dim = Win32GetWindowDimension( WindowHandle );
				Win32DisplayBufferInWindow( &GlobalBackBuffer, DeviceContext, Dim.Width, Dim.Height );
			}
			//ReleaseDC( WindowHandle, DeviceContext );
		}
		else
		{
// TODO(andrew):	Diagnostic
		}
	}
	else
	{
//	TODO(andrew):	Diagnostic
	}

	return(0);
}



#endif // _WIN32   END OF MICROSOFT WINDOWS SPECIFIC CODE
