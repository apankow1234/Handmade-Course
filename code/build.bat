@echo off

mkdir ..\..\build
pushd ..\..\build

cl /FC /F4194304 /Zi ..\Handmade-Course\code\win32_handmade.cpp /link user32.lib Gdi32.lib

popd
